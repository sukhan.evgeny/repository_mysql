package sukhan.mycompany.connector;

import java.sql.Connection;

public interface ConnectionProvider {
    Connection getConnection();
}
