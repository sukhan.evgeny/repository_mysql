package sukhan.mycompany.repository;

import sukhan.mycompany.entity.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ProjectRepository extends AbstractRepository {

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    private List<Project> projects = new ArrayList<>();

    public Project create (Project project) throws SQLException {
        final String sql = "INSERT INTO 'app_project' ('id', 'name', 'description') VALUES (?, ?, ?)";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setLong(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.execute();
        return project;
    }

    public void create(final String name) throws SQLException {
        create(new Project(name));
    }

    public void create(final String name, final String description) throws SQLException {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        create(project);
    }

    public void update(Project project) throws SQLException {
        final String sql = "UPDATE 'app_project' SET 'name' = ?, 'description' = ? " +
                            "WHERE 'id' = ?";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setLong(3, project.getId());
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.execute();
    }

    public void clear() throws SQLException {
        final String sql = "DELETE FROM 'app_project'";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.execute();
    }

    @Deprecated
    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size() -1) return null;
        return projects.get(index);
    }

    public Project findByName(final String name) throws SQLException {
        final String sql = "SELECT FROM 'app_project' WHERE 'name' = ?";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return fetch(resultSet);
    }

    public Project findById(final Long id) throws SQLException {
        final String sql = "SELECT FROM 'app_project' WHERE 'id' = ?";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return fetch(resultSet);
    }

    @Deprecated
    public Project removeByIndex(final int index) {
        final Project task = findByIndex(index);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    public void removeById(final Long id) throws SQLException {
        final String sql = "DELETE FROM 'app_project' WHERE 'id' = ?";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setLong(1, id);
        statement.execute();
    }

    public void removeByName(final String name) throws SQLException {
        final String sql = "DELETE FROM 'app_project' WHERE 'name' = ?";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setString(1, name);
        statement.execute();
    }

    public List<Project> findAll() throws SQLException {
        final String sql = "SELECT FROM 'app_project'";
        final PreparedStatement statement = getConnection().prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        List<Project> project = new ArrayList<>();
        while (resultSet.next()) project.add(fetch(resultSet));
        return project;
    }

    private Project fetch(ResultSet resultSet) {
        return null;
    }

//    public static void main(String[] args) {
//        final ProjectRepository projectRepository = new ProjectRepository();
//        System.out.println(projectRepository.findAll());
//        projectRepository.create("DEMO");
//        projectRepository.create("TEST");
//        System.out.println(projectRepository.findAll());
//        projectRepository.clear();
//        System.out.println(projectRepository.findAll());
//    }

}
