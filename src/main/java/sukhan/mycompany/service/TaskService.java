package sukhan.mycompany.service;

import sukhan.mycompany.connector.ConnectionProvider;
import sukhan.mycompany.entity.Task;
import sukhan.mycompany.repository.ProjectRepository;
import sukhan.mycompany.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final ConnectionProvider provider;

    private TaskRepository getRepository() {
        return new TaskRepository(provider.getConnection());
    }

    public TaskService(ConnectionProvider provider) {
        this.provider = provider;
    }

    public Task create(String name) {
        return getRepository().create(name);
    }

    public Task findByIndex(int index) {
        return getRepository().findByIndex(index);
    }

    public Task findByName(String name) {
        return getRepository().findByName(name);
    }

    public Task removeById(Long id) {
        return getRepository().removeById(id);
    }

    public Task removeByName(String name) {
        return getRepository().removeByName(name);
    }

    public Task findById(Long id) {
        return getRepository().findById(id);
    }

    public void clear() {
        getRepository().clear();
    }

    public List<Task> findAll() {
        return getRepository().findAll();
    }

}
