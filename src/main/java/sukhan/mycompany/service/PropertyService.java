package sukhan.mycompany.service;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService {
    private static final String prop = "/application.properties";
    private static InputStream input = PropertyService.class.getResourceAsStream(prop);
    private static final Properties PROPERTIES = new Properties();
    {
        try {
            PROPERTIES.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getJdbcPort() {
        return PROPERTIES.getProperty("jdbc.port");
    }
    public String getJdbcHost() {
        return PROPERTIES.getProperty("jdbc.host");
    }
    public String getJdbcUsername() {
        return PROPERTIES.getProperty("jdbc.username");
    }
    public String getJdbcPassword() {
        return PROPERTIES.getProperty("jdbc.password");
    }
    public String getJdbcDb() {
        return PROPERTIES.getProperty("jdbc.database");
    }
    public String getJdbcURL() {
        return PROPERTIES.getProperty("jdbc.url") + getJdbcHost()
            + ":" + getJdbcPort() + "/" + getJdbcDb();
    }

}
