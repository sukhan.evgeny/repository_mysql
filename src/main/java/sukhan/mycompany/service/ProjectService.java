package sukhan.mycompany.service;

import com.mysql.jdbc.PreparedStatement;
import sukhan.mycompany.connector.ConnectionProvider;
import sukhan.mycompany.entity.Project;
import sukhan.mycompany.repository.ProjectRepository;

import java.sql.SQLException;
import java.util.List;

public class ProjectService {

    private final ConnectionProvider provider;

    private ProjectRepository getRepository() {
        return new ProjectRepository(provider.getConnection());
    }

    public ProjectService(ConnectionProvider provider) {
        this.provider = provider;
    }

    public void create(String name) throws SQLException {
        getRepository().create(name);
    }

    public void create(String name, String description) throws SQLException {

        getRepository().create(name, description);
    }

    public void update(Long id, String name, String description) throws SQLException {
        getRepository().update(new Project(id, name, description));
    }

    public void clear() throws SQLException {
        getRepository().clear();
    }

    public Project findByIndex(int index) {
        return getRepository().findByIndex(index);
    }

    public Project findByName(String name) throws SQLException {
        return getRepository().findByName(name);
    }

    public Project findById(Long id) throws SQLException {
        return getRepository().findById(id);
    }

    public Project removeByIndex(int index) {
        return getRepository().removeByIndex(index);
    }

    public void removeById(Long id) throws SQLException {
        getRepository().removeById(id);
    }

    public void removeByName(String name) throws SQLException {
        getRepository().removeByName(name);
    }

    public List<Project> findAll() throws SQLException {
        return getRepository().findAll();
    }

//    public static void main(String[] args) {
//        ProjectRepository.main(args);
//    }

}
